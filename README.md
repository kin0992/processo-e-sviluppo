# Assignment 3 - STRUTS application

### Comi Marco, 746511
Versione Apache Tomcat usata: 8.5

Versione Java usata: Java 8

## Settaggio progetto

Importare il progetto in Eclipse (File->Import->Project from folder/archive)

Se son presenti errori, è perchè manca la libreria legata al server: aggiungerla al build-path in base alla versione che si ha

Cliccare con il tasto destro sul progetto, configura build-path, cliccare poi su aggiungi libreria e selezionare server runtime library.

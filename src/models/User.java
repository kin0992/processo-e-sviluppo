package models;

public class User {
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private String email;
	private String city;
	private String type;
/**
 * Constructor used in the registration form
 * @param name User's first name
 * @param lastName User's last name
 * @param username User's username
 * @param password User's password
 * @param email User's email address
 * @param city User's city
 */
	public User(String name, String lastName, String username, String password, String email, String city) {
		this.firstName = name;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.email = email;
		this.city = city;
		this.type = "normal";
	}
/**
 * Constructor userd to create users already present in a simulate a DB
 * @param name User's first name
 * @param lastName User's last name
 * @param username User's username
 * @param password User's password
 * @param email User's email address
 * @param city User's city
 * @param type User's type
 */
	public User(String name, String lastName, String username, String password, String email, String city,
			String type) {
		this.firstName = name;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.email = email;
		this.city = city;
		this.type = type;
	}
/**
 * 
 * @return First name of the user
 */
	public String getFirstName() {
		return firstName;
	}
/**
 * 
 * @param name User's first name
 */
	public void setFirstName(String name) {
		this.firstName = name;
	}
/**
 * 
 * @return Last name of the user
 */
	public String getLastName() {
		return lastName;
	}
/**
 * 
 * @param lastName User's last name
 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
/**
 * 
 * @return The username of the user
 */
	public String getUsername() {
		return username;
	}
/**
 * 
 * @param username  User's username
 */
	public void setUsername(String username) {
		this.username = username;
	}
/**
 * 
 * @return The hashed password of the user
 */
	public String getPassword() {
		return password;
	}
/**
 * 
 * @param password  User's password
 */
	public void setPassword(String password) {
		this.password = password;
	}
/**
 * 
 * @return The email of the user
 */
	public String getEmail() {
		return email;
	}
/**
 * 
 * @param email  User's email address
 */
	public void setEmail(String email) {
		this.email = email;
	}
/**
 * 
 * @return The city of the user
 */
	public String getCity() {
		return city;
	}
/**
 * 
 * @param city  User's city
 */
	public void setCity(String city) {
		this.city = city;
	}
/**
 * 
 * @return The type of the user
 */
	public String getType() {
		return type;
	}
/**
 * 
 * @param type  User's type: normal or admin
 */
	public void setType(String type) {
		this.type = type;
	}

}

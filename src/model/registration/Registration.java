package model.registration;

import static persistence.FakePersistentData.usersList;

import models.User;

public class Registration {
/**
 * 
 * @param user User object
 * @return True if the user already exists, false otherwise
 */
	public static boolean checkIfUserAlreadyExists(User user) {
		for (User registeredUser : usersList) {
			if (user.getEmail().equals(registeredUser.getEmail())
					|| user.getUsername().equals(registeredUser.getUsername()))
				return true;
		}
		return false;
	}
/**
 * Add a new user to the users' list
 * @param user User's object 
 */
	public static void addNewUserToUsersList(User user) {
		usersList.add(user);
	}

}

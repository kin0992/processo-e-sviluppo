package model.authentication;

import models.User;
import utils.UserUtils;

public class Login {

	private User authUser;
/**
 * Create the Login object for the user who is doing the login action
 * @param user User's data
 */
	public Login(User user) {
		this.setAuthUser(user);
	}
/**
 * Authenticate the user
 * @param username Username got from the login form
 * @param password Plain password got from the login form
 * @return True if the user is correctly authenticated, false otherwise
 */
	public boolean autheticate(String username, String password) {
		if (this.authUser.getUsername().equals(username) && UserUtils.checkPassword(authUser, password)) {
			return true;
		} else {
			return false;
		}
	}
/**
 * Get the user's data
 * @return The user in authentication process
 */
	public User getAuthUser() {
		return authUser;
	}
/**
 * Create the user object
 * @param authUser User's data
 */
	public void setAuthUser(User authUser) {
		this.authUser = authUser;
	}

}

package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import models.User;
import security.CryptingAlgorithm;

public class UserUtils {
	/**
	 * Generate the password for the user
	 * 
	 * @param user
	 *            User's data
	 */
	public static void generatePassword(User user) {
		user.setPassword(generateHashedPassword(user.getPassword()));

	}

	/**
	 * 
	 * @param user
	 *            User's object
	 * @param password
	 *            Password got from the login form
	 * @return True if the passwords match, false otherwise
	 */
	public static boolean checkPassword(User user, String password) {
		return StringUtils.equals(user.getPassword(), generateHashedPassword(password));
	}

	/**
	 * Hash the user's password
	 * 
	 * @param password
	 *            Password got from the form
	 * @return The hashed password
	 */
	private static String generateHashedPassword(String password) {
		String hashedPassword = "StruTS123?!" + password;
		return CryptingAlgorithm.generateHash(hashedPassword);
	}

	/**
	 * Validate the email address
	 * 
	 * @param email
	 *            Email address that needs to be validate
	 * @return True if the address is valid, false otherwise
	 */
	public static boolean validateEmailAddress(String email) {
		String regex = "^(.+)@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

}

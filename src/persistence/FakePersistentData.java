package persistence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import models.User;

public class FakePersistentData {
	//username: pigi, password: posso
	public static User adminUser1 = new User("pino", "gino", "pigi", "f2e333ec83a84a94a298c4440d9375fc20628a77", "email.falsa@fake.mail", "NY", "admin");
	//username: barba, password: barba
	public static User adminUser2 = new User("james", "harden", "barba", "d06f35b446764cb909595d7d95b6ac782485d744", "fear.beard@rockets.com", "Houston", "admin");
	//username: sancho, password: panza
	public static User normalUser1 = new User("sancho", "panza", "sancho", "cf300272dfb630dcd20576e373e95efe53c6f3fc", "sancho@panza.com", "Madrid", "normal");
	public static  List<User> usersList = new ArrayList<User>(Arrays.asList( adminUser1, adminUser2, normalUser1));
	
	/**
	 * Get the user from the users list; select the user from the username
	 * @param username Username of the user we need
	 * @return User's object which contains all the user's data if exists, null otherwise
	 */
	public static User getUserData(String username) {
		for (User user: usersList) {
			if (user.getUsername().equals(username))
				return user;
		}
		return null;
	}
	
	/**
	 * Get the list of users
	 * @return The list of all users
	 */
	public List<User> getUsers(){
		return usersList;
	}
}

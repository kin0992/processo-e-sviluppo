package actions.registration;

import com.opensymphony.xwork2.ActionSupport;

import model.registration.Registration;
import models.User;
import utils.UserUtils;

public class RegistrationAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private String name;
	private String lastname;
	private String username;
	private String password;
	private String email;
	private String city;

	/**
	 * 
	 * @return The first name that the user used in the registration process
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            User's first name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The last name that the user used in the registration process
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * 
	 * @param lastname
	 *            User's last name
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * 
	 * @return The username that the user used in the registration process and
	 *         will use for the login process
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 
	 * @param username
	 *            User's username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 
	 * @return The hashed password of the user
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password
	 *            User's password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * @return The email that the user used in the registration process
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 *            User's email address
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return The city that the user used in the registration process
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 
	 * @param city
	 *            User's city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Validate mandatory fields for the registration process
	 */
	public void validate() {
		User inRegistrationUser = new User(this.name, this.lastname, this.username, this.password, this.email,
				this.city);
		if (inRegistrationUser.getUsername().trim().equals(""))
			addFieldError("username", "You must insert the username");
		if (inRegistrationUser.getEmail().trim().equals(""))
			addFieldError("email", "You must insert the email");
		else if (UserUtils.validateEmailAddress(inRegistrationUser.getEmail()) == false)
			addFieldError("email", "Insert a valid email address");
		if (inRegistrationUser.getPassword().length() < 3) {
			if (inRegistrationUser.getPassword().length() == 0)
				addFieldError("password", "You must insert the password");
			else if (inRegistrationUser.getPassword().trim().equals(""))
				addFieldError("password", "Come on, don't joke with me, chose a real password");
			else
				addFieldError("password", "Password must be at least long 3 characters");
		} else {
			if (inRegistrationUser.getPassword().trim().equals(""))
				addFieldError("password", "Come on, don't joke with me, chose a real password");
		}
	}

	/**
	 * Execute operations for the registration action
	 * 
	 * @return The name of the action that STRUTS will map for the registration
	 *         action
	 */
	public String execute() {
		User inRegistrationUser = new User(this.name, this.lastname, this.username, this.password, this.email,
				this.city);
		if (Registration.checkIfUserAlreadyExists(inRegistrationUser)) {
			addActionError(getText("error.registration"));
			return "alreadyExists";
		} else {
			UserUtils.generatePassword(inRegistrationUser);
			Registration.addNewUserToUsersList(inRegistrationUser);
			return "userCreated";
		}
	}

}

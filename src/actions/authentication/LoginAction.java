package actions.authentication;

import static persistence.FakePersistentData.getUserData;

import com.opensymphony.xwork2.ActionSupport;

import model.authentication.Login;
import models.User;

public class LoginAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	// Data coming from the form, automatically injected by STRUTS
	private String username;
	private String password;

	/**
	 * 
	 * @return The username from the login form
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 
	 * @param username
	 *            User's username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 
	 * @return The hashed password of the user
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password
	 *            User's password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Check if the mandatory params are setted
	 * 
	 */
	public void validate() {
		if (this.username.trim().equals(""))
			addFieldError("username", "You must insert the username");
		if (this.password.length() == 0)
			addFieldError("password", "You must insert the password");
	}

	/**
	 * Execute operations for the login action
	 * 
	 * @return The name of the action that STRUTS will map for the login action
	 */
	public String execute() {
		User toAuthenticateUser = getUserData(username);
		if (toAuthenticateUser != null) {
			Login login = new Login(toAuthenticateUser);
			if (login.autheticate(username, password)) {
				if (toAuthenticateUser.getType().equals("admin")) {
					return "admin";
				} else {
					return "success";
				}
			} else {
				addActionError(getText("error.login"));
				return "error";
			}
		} else {
			addActionError(getText("invalid.login"));
			return "error";
		}

	}
}

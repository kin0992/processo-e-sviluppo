<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sign up form</title>
</head>
<body>

<h2>Sign up</h2>
<p>Already have an account? Go to <a href=login.jsp>login</a> page</p>
<s:actionerror />
<s:form action="registration.action" method="post">
	<s:textfield name="name" key="label.name" size="20"/>
	<s:textfield name="lastname" key="label.lastname" size="20"/>
	<s:textfield name="username" key="label.username" size="20"/>
	<s:password name="password" key="label.password" size="20"/>
	<s:textfield name="email" key="label.email" size="20"/>
	<s:textfield name="city" key="label.city" size="20"/>
	
	<s:submit method="execute" key="label.registration" align="center"/>
	<s:reset>Reset</s:reset>
</s:form>

</body>
</html>
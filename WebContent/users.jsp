<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin - Users list</title>
</head>
<body>

	<h2>Users list</h2>
	<s:iterator value="@persistence.FakePersistentData@usersList"
		var="users">
		<ol>
			<s:label>User</s:label>
			<strong><s:property value="#users.username" /></strong>:
			<ul>
				<s:if test="%{#users.firstName!=''}">
					<li>First name: <s:property value="#users.firstName" /></li>
				</s:if>
				<s:if test="%{#users.lastName!=''}">
					<li>Last name: <s:property value="#users.lastName" /></li>
				</s:if>
				<s:if test="%{#users.email!=''}">
					<li>Email: <s:property value="#users.email" /></li>
				</s:if>
				<s:if test="%{#users.city!=''}">
					<li>City: <s:property value="#users.city" /></li>
				</s:if>
				<s:if test="%{#users.type!=''}">
					<li>User type: <s:property value="#users.type" /></li>
				</s:if>
			</ul>
		</ol>
	</s:iterator>
	<s:actionerror />

</body>
</html>
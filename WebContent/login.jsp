<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Processo e Sviluppo - 3rd Assignment</title>
</head>
<body>

<h2>Login</h2>
<s:actionerror />
<p>Don't have an account? <a href="registration.jsp">Sign up</a> now!</p>
<s:form action="login.action" method="post">
	<s:textfield name="username" key="label.username" size="20"/>
	<s:password name="password" key="label.password" size="20"/>
	
	<s:submit method="execute" key="label.login" align="center"/>
</s:form>

</body>
</html>